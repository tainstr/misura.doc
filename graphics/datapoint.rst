.. include:: ../substitutions.txt

.. _datapoint:

|dp|
=====

A |dp| represents an highlighted point on a plotted curve. 
It is subordered to that curve and strictly bound to its fate. If the curve is deleted, the |dp| gets deleted also. 
If some aspect of the curve change, like the x or y dataset or axis, the |dp| is adjusted accordingly. 

Creating a |dp|
-----------------------

There are two methods of placing a new |dp| on a curve:

1. Activate the *picker* tool and click on a curve. A new |dp| will be created, associated to the curve you clicked onto, 
   and as near as possible to the coordinates where your click happened. 
2. Select the curve in the :ref:`navigator`, then right click on its node and select the :menuselection:`Intercept` action. 
   A plugin window will allow to customize the location where the |dp| will appear and some additional settings.
   
Positioning
-----------

Once created, a |dp| can be positioned by dragging and dropping it along its curve (it will never *jump* to another curve). 

Its exact positioning, once dropped, can follow different algorithms, which makes it a powerful analytical tools. 
For example, a |dp| can search for the nearest maximum, minimum or inflection point in the target curve.

Placement options can be configured before creating a datapoint according to method (2) above (:menuselection:`Intercept` from :ref:`navigator`) 
by setting the :guilabel:`Place nearest` field.
Once the |dp| has been created with either method, placement algorithm can be modified from the :guilabel:`Place nearest` setting
found in the :guilabel:`Properties` panel.  

#. :guilabel:`Nearest (fixed X)`: the default method will consider the X coordinate of your click, and place the |dp| the nearest possible to that value, 
   without considering the Y coordinate.
#. :guilabel:`Nearest`: as above, but will consider also the Y coordinate. Use this method when there is a non-univocal curve (multiple Y for the same X).
#. :guilabel:`Maximum`: search the nearest maximum value
#. :guilabel:`Minimum`: as above, looking for the minimum
#. :guilabel:`Inflection`: search the nearest point where the second derivative is zero
#. :guilabel:`Stationary`: as above, but use the first derivative

Inflection and Stationary methods will need an addtional X dataset in order to calculate the derivatives. 
The |dp| is initialized with the same X dataset of the parent curve, but that can be changed in the :guilabel:`Critical search X dataset` setting.

The surrounding portion of the curve, in which the automatic positioning will occur, is defined by :guilabel:`Critical search range` option. 
It is expressed as the number of points around the current position of the |dp| where to search according to the selected method. 
This allows a precise *local* search of the desired morphological feature of the curve.

Labels
--------

|dp| creates a subordered label listing the Y value, temperature and time of the point. The label can be deleted 
by setting :guilabel:`Show label` to ``False`` under the :guilabel:`Formatting` panel of the |dp|.

The label can be customized in :guilabel:`Output label text` folling Python replacement rules. Available substitutions are:

- ``y``: y value (``%(y).1f`` will output ``1.2`` if y is 1.234)
- ``x``: x value, as above
- ``ylabel``: the label of the y axis, use the string formatter: ``%(ylabel)s``
- ``xlabel``: x axis label, as above
- ``t``: time value
- ``T``: temperature value

For example:

	``Intercept\\%(ylabel)s=%(y).1E\\T=%(T).1f\\t=%(t)i``
	
Will render as:

|	Intercept
|	Vol (%)=5.1E
|	T=840.1
|	t=2800

Lines
-----------

The :guilabel:`Formatting` panel offers three kinds of subordered lines:

#. :guilabel:`Show tangent line`: a line tangent to the curve will be added
#. :guilabel:`Show perpendicular line`: a line perpendicular to the curve will be added
#. :guilabel:`Second Data Point`: specify a second |dp|: a line will be drawn which passes through both the current |dp| and that second |dp|.

After one of these option is activated, it might be necessary to move the |dp| on the plot for the related lines to show up. 
Alternatively, you can click the :guilabel:`Update Data Point` button a few times.

Remove Gaps
--------------

A |dp| can also be placed on curve *defects*, like sudden jumps, in order to remove them by clicking on the :guilabel:`Remove Gaps` button. 
The curve will be *smoothed* around the datapoint, removing sudden jumps. You can use the :guilabel:`Undo` button to revert its operation.

Gaps removal algorithm reads two |dp| settings which can be properly configured to obtain the desired output:

#. :guilabel:`Remove gaps range`: Number of points surrounding the |dp| that will be scanned for jump removal.
#. :guilabel:`Remove gaps threshold`: A limit above which the distance between two points is considered a jump that should be removed.

The threshold is automatically configured according to the range and the selected surrounding, so that any point above the mean scattering should be *flattened* out. 


