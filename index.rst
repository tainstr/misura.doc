.. Misura documentation master file, created by
   sphinx-quickstart on Tue Mar 17 11:03:10 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: substitutions.txt

|m| documentation project
=======================================

The Misura software was abandoned in 2019 and was forked by the MDF project by Expert Lab Service.

   `<http://www.expertlabservice.it/en/software/>`_

|m| was the data acquisition and analysis software platform for non-contact high temperature measurement instruments.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install
   components
   instruments
   browser/browser.rst
   graphics
   m3
   flash/flash.rst
   access
   server
   glossary

.. todo: reinsert options.rst after committing all autodoc options/* files




