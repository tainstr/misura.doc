.. Misura Flash documentation master file, created by
   sphinx-quickstart on Fri Feb  2 11:15:26 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../substitutions.txt

|mf| documentation
========================================

|mf| is a post-analysis tool for reading, organizing and processing data obtained from 
FlashLine instrument control software running on TA Instrument's flash products. 

|mf| extends the functionality of |m4| software: it is build upon the common platform explained in the general documentation.

It currently supports the calculation of thermal diffusivity from flash thermograms, 
according to advanced curve fitting models as: 

- :ref:`model_gembarovic`
- :ref:`model_inplane`
- :ref:`model_multilayer` (two and three layers)

After a new |m| installation, the Flash functionality is deactivated by default. 
To activate it:

#. open the :ref:`browser`
#. from the menubar select :menuselection:`Help --> Client configuration`
#. Navigate to the :guilabel:`Data import`
#. Write the word ``thegram`` in the :guilabel:`Import plugins by name` option
#. Click upper-right :guilabel:`Save` button
#. Close and reopen the :ref:`browser`


.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   quickstart
   data_import
   wizard_help
   interface
   models
   flash_options


