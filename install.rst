.. include:: substitutions.txt

.. _install:

Installation
==================================

This document describes the installation and quick start of |m4| software.

Prerequisites
---------------

- 2GB of free hard disk storage
- At least 500MB of free RAM at runtime (see below)
- 2Ghz, dual core processor
- Maximum screen resolution: 96dpi. 

For windows:
- Windows 10 64bit
- 4GB RAM for ODP users
- 8GB RAM for Flash users

For linux:
- Ubuntu 16.04
- 2GB RAM for general users 
- 8GB for Flash users


Dependencies:

- The latest Windows Visual C++ 2008 Redistributable package, x86 version (independently on your OS being 64 or 32 bits): `vcredist`_.
- You will need `Xvid`_ MPEG4 video compression filter in order to export compressed videos of recorded frames.

Known incompatibilities:

- High DPI screens might not be supported and cause Misura to crash. Please 
rescale to 96 DPI.

.. _vcredist: https://www.microsoft.com/en-us/download/details.aspx?id=29
.. _Xvid: https://www.xvid.com/download/


|win| 10 64bit 
----------------
|m4| for |win| 1064bit can be obtained as a `standard installer` (``.exe``).

Installation initiates by double clicking on the installer package proceeding through the pages. 
Any previous installation will be deleted, but local Misura data and configuration will not be affected.

The installer will ask which components to install. Misura Flash users can skip the 
installation of Acquisition and Graphics components.

.. _standard installer: https://bitbucket.org/tainstr/misura.client/downloads

.. _upgrade:

Upgrading
^^^^^^^^^^

The :ref:`browser` menu action :menuselection:`Help --> Check client updates` will check if any newer client version is available, 
download the installer and start the installation procedure.

The :ref:`live_acquisition` window also offers :menuselection:`Help --> Check server updates` when connected to an instrument. 
This action upgrades the instrument's firmware, closes the client and restarts the server. 


|u|
------
|m4| for |ults| GNU/Linux operative system should be installed along with the full development environment.

Follow instructions contained in the `README`_.

Both client and server upgrades are available as explained above, :ref:`upgrade`.

.. _README: https://github.com/tainstr/misura.client
.. _public repository: https://github.com/tainstr/misura.client/releases


Bug reports and support
------------------------

For assistance you can contact Daniele Paganelli, d paganelli at tainstruments .com. 

The **Help** menu of :ref:`browser` and :ref:`live_acquisition` contains a **Bug report** action which will create a zip archive 
containing useful information for debugging, :menuselection:`Help --> Bug report`. It is recommended to attach this archive to any support request.

- If the issue involves running a test or controlling an instrument, the bug report should be created from :ref:`live_acquisition` while connected to the machine. 
  In this way also instrument logs will be included.  
- If the issue involves only data processing and visualization, the bug report can be created from the :ref:`browser`.
- If doubts about measurements are implied, please send also relevant test files.  

It is advisable to set the client bug level filter to 0, so all user interface messages will be logged (``Logging level`` option in :ref:`preferences`).
